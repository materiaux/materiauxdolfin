# v0.3: Update for compatibility with materiaux v0.3

 * compatibility updates
 * test fixes
 * improved handling of initial states in class `GSMForms`


# v0.2: Update for compatibility with materiaux v0.2

 * Minor Python user interface changes. The test have been updated accordingly which can serve as an example for interested users.



# v0.1: initial release

 * functionality is there, actively used for research
