# materiauXdolfin - FEniCS/dolfin integration of `materiaux`

This package is a companion of [`materiaux`](https://gitlab.com/materiaux/materiaux) that integrates it into FEniCS/dolfin via the package
[`dolfincoefficients`](https://gitlab.com/materiaux/dolfincoefficients). Please note that this project is tailored to legacy FEniCS/dolfin and thus 
should be considered as legacy as well. Development efforts rather go into 
materiauXdolfinx.


## Installation

Standard Python setuptools installation. Requires `materiaux` version >=0.1 and `dolfincoefficients` version >= 0.1.

Ready-to-run containers can be found in the [container registry](https://gitlab.com/materiaux/containers/container_registry/) 
of [`Materiaux Containers`](https://gitlab.com/materiaux/containers).


## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

