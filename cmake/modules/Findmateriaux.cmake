
execute_process(COMMAND python3 ${CMAKE_SOURCE_DIR}/cmake/modules/detect_materiaux.py --includes
                RESULT_VARIABLE result
                OUTPUT_VARIABLE MATERIAUX_INCLUDE_DIR)

if (NOT ${result} EQUAL "0")
    unset(MATERIAUX_INCLUDE_DIR)
endif ()

execute_process(COMMAND python3 ${CMAKE_SOURCE_DIR}/cmake/modules/detect_materiaux.py --version
        RESULT_VARIABLE result
        OUTPUT_VARIABLE MATERIAUX_VERSION)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(materiaux
        FOUND_VAR MATERIAUX_FOUND
        REQUIRED_VARS
        MATERIAUX_INCLUDE_DIR
        VERSION_VAR MATERIAUX_VERSION
        )
