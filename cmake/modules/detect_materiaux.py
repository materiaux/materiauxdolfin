#! /usr/bin/env/python3

import sys
import pathlib
import argparse
import materiaux


def print_includes():
    print(materiaux.include_directory, end="")


def print_version():
    import materiaux.version
    print(materiaux.version.__version__, end="")


parser = argparse.ArgumentParser()
group = parser.add_mutually_exclusive_group()
group.add_argument("--includes", help="print include directory", action="store_true")
group.add_argument("--version", help="print version", action="store_true")

args = parser.parse_args()
if args.includes:
    print_includes()
elif args.version:
    print_version()
