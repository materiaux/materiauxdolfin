#! /usr/bin/env python3

import os
import pathlib

pwd = pathlib.Path.cwd()

print("\n")
print("Creating Python documentation in {!s}".format(pwd / "build" / "html"))
print("*"*80 + "\n")

ret = os.system("sphinx-apidoc -o source ../materiauXdolfin")
if ret != 0:
    raise Exception("Some error occurred during api doc generation.")
    
ret = os.system("make html")
if ret != 0:
    raise Exception("Some error occurred during sphinx doc build.")
