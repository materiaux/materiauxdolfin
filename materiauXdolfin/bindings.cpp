// Copyright (C) 2019-2020 Matthias Rambausek
//
// This file is part of 'materiaux'
// (https://gitlab.com/materiaux/materiauxdolfin)
//
// SPDX-License-Identifier:    AGPL-3.0-or-later

#include <dolfincoefficients.h>
#include <functional>
#include <materiaux_module.h>
#include <pybind11/pybind11.h>

namespace py = pybind11;

PYBIND11_MODULE(cpp, m) {
  m.def("create_local_function", [](const materiaux::Function<double> &func) {
    return dolfincoefficients::LocalFunction{
      func.func, static_cast<size_t>(materiaux::result_size<double>(func)),
        static_cast<size_t>(materiaux::active_coefficients_size<double>(func)),
        static_cast<size_t>(materiaux::constants_size<double>(func))};
  });
}
