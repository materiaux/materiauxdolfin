# Copyright (C) 2019-2021 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/materiauxdolfin)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later

import typing
import dolfin
import ufl.core.expr
import dolfincoefficients.cpp
import dolfincoefficients.factories
from dolfincoefficients.factories import mk_ufl_from_dolfin_cpp_element
import materiauXdolfin.factories as function
import materiaux.cpp
from materiaux import FieldData
from .cpp import create_local_function


def create_quadrature_element(field_data: FieldData, cell: ufl.Cell, quadrature_degree: int,
                              quad_scheme="default"):
    """
    Creates a "Quadrature" Element based on the given cell and quadrature parameters
    """
    if len(field_data.shape) == 0 or (len(field_data.shape) == 1 and field_data.shape[0] == 1):
        return dolfin.FiniteElement("Quadrature", cell, degree=quadrature_degree, quad_scheme=quad_scheme)
    else:
        if len(field_data.symmetry) == 0:
            symmetry = None
        elif len(field_data.symmetry) == 1:
            symmetry = {tuple(kk): tuple(vv) for kk, vv in field_data.symmetry[0] if len(vv) > 0}
            if len(symmetry) == 0:
                symmetry = None
        elif len(field_data.symmetry) > 1:
            for rank in field_data.symmetry:
                for symm in rank:
                    try:
                        if len(symm[0]) > 0:
                            raise NotImplementedError("Symmetry information for more than one rank is not implemented.")
                    except IndexError:
                        pass
            symmetry = None
        else:
            raise RuntimeError("Unexpected symmetry information.")

        if len(field_data.shape) == 1 and symmetry is None:
            return dolfin.VectorElement("Quadrature", cell, degree=quadrature_degree, quad_scheme=quad_scheme,
                                        dim=field_data.shape[0])
        else:
            return dolfin.TensorElement("Quadrature", cell, degree=quadrature_degree, quad_scheme=quad_scheme,
                                        shape=tuple(field_data.shape), symmetry=symmetry)


def create_coefficient(field_data: FieldData, mesh: dolfin.Mesh, quadrature_degree: int,
                       quad_scheme="default",
                       name: str = None) -> dolfincoefficients.cpp.Coefficient:
    """
    Creates dolfin.Function on a "Quadrature" space
    """
    V = dolfin.FunctionSpace(
        mesh,
        create_quadrature_element(field_data, mesh.cell_name(), quadrature_degree, quad_scheme=quad_scheme)
    )
    return dolfincoefficients.factories.create_coefficient(V, name=name)


def create_compiled_quadrature_expression(func: materiaux.cpp.double.Function, target_element: ufl.FiniteElementBase,
                                          all_coefficients: typing.Dict[str, typing.Union[
                                              dolfincoefficients.cpp.Coefficient, dolfin.Function, ufl.core.expr.Expr]],
                                          all_constants: typing.Dict[
                                              str, dolfin.Constant],
                                          form_compiler_parameters: dict = None,
                                          **compiled_expression_kwargs) -> dolfin.CompiledExpression:
    """
    Creates a compiled quadrature expression from a materiaux.cpp.double.Function and dolfin data.
    """
    return dolfincoefficients.factories.create_compiled_quadrature_expression(create_local_function(func), target_element,
                                                                              [all_coefficients[coeff.name] for coeff in
                                                                            materiaux.cpp.active_coefficients(func)],
                                                                              [all_constants[constant.name] for constant in
                                                                            func.constant_data],
                                                                              form_compiler_parameters=form_compiler_parameters,
                                                                              **compiled_expression_kwargs)
