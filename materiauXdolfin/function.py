# Copyright (C) 2019-2020 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/materiauxdolfin)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later


"""This module only import from dolfincoefficients.function"""


from dolfincoefficients.function import *
