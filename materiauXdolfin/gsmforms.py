# Copyright (C) 2019-2021 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/materiauxdolfin)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later

import typing
import ufl
import dolfin
from materiaux import FieldData
import materiaux.cpp as cpp
import materiauXdolfin.factories as factories
import materiauXdolfin.function as function


def _return_not_none(val):
    if val is not None:
        return val
    else:
        raise RuntimeError("Value is unexpectedly None.")


def create_linearization(material_response, evolution_equations=None) -> cpp.double.Function:
    """
    Creates the materiaux.cpp.double.Function that represents the lienarization of the given material response
    respecting the optionally provided evolutions equations.

    :param material_response: In case evolution equations is 'None', material_response must have an attribute named
        'd_ext_duals' which is a materiaux.cpp.double.Function that represents the linearization of the duals
        (= material response fields to duals to the primal fields; ie. stress as dual to strain) with repsect to the
        primal fields of the model. When evolution_equations is provided, then 'material_response' is required to have
        and additional attribute 'd_int_duals', that is the linearization of the duals with respect to the internal
        variables of which the evolution is governed by the given evolution equations.
    :param evolution_equations: analogous to 'material_response', 'evolution_equations' has to have attributes
        'd_ext_eqs' and 'd_int_eqs'.
    :return: the linearization of the model.
    """
    if evolution_equations is not None:
        return cpp.create_consistent_linearization(
            material_response.d_ext_duals, material_response.d_int_duals,
            evolution_equations.d_ext_eqs, evolution_equations.d_int_eqs
        )
    else:
        return material_response.d_ext_duals


def create_evolution(evolution_equations, solver: cpp.double.NonlinearSolver = None, throw_on_error: bool = False) \
        -> cpp.double.Function:
    """
    Creates a materiaux.cpp.double.Function that computes the new internal state by solving the given evolution
    equations.
    
    :param evolution_equations: an object with attributes 'eqs' that is a Function computing the residual and
        'd_int_eqs' that is a Function computing the linearization of 'eqs' with respect ot the internal state, which
        is solved for.
    :param solver: a solver routine that is employed to solve the evolution equations
    :param throw_on_error: Throw on failure when trying to solve the evolution equations. Otherwise, evolution will
        yield NaNs to indicate an error.
    :return: Function that computes the new internal states
    """
    solver = solver if solver is not None else cpp.create_newton_raphson(cpp.DOUBLE)
    return cpp.create_evolution(evolution_equations.eqs, evolution_equations.d_int_eqs, solver, throw_on_error)


def _check_factory_fields(fields: typing.Dict[str, typing.Any], required_fields: typing.Dict[str, cpp.FieldData]):
    """
    Helper function to check whether all items in 'required_fields' have some corresponding object in 'fields'.
    Raises a ValueError when this is not the case.
    """
    _fields_not_found = []
    for field in filter(lambda fd: fd.type not in ("Parameter", "InternalState", "HistoryState"),
                        required_fields.values()):
        if field.name not in fields.keys():
            _fields_not_found.append(field)
    if len(_fields_not_found) != 0:
        fields_str = ", ".join(["{:s} -> {:s}".format(field.name, field.type) for field in _fields_not_found])
        raise ValueError("Required fields ({:s}) have not been provided.".format(fields_str))


class GSMForms:
    """
    Abstract base class that defines the interface methods 'Pi', 'G', 'G_lin' and 'duals', that return FEniCS
    UFL/dolfin forms. Moreover, GSMForms provides good deal of operations under the hood like updating internal
    variables and computing consistent linearization. For a n idea how to use this class, have a look at the test cases.

    A note on history states and time: the implementation provided assumes that the time field is named "t" and its
    history is "t_n". The same scheme applies to internal states and their history states, which is in agreement with
    the conventions followed by materiaux.modeling.gsm. However, the behavior can be overridden by derived classes.
    """

    @classmethod
    def mk_factory(cls, fields: typing.Dict[str, typing.Any], mesh: dolfin.Mesh, *args,
                   field_data: typing.Dict[str, FieldData] = None, form_compiler_parameters: dict = None,
                   **kwargs) -> typing.Callable:
        """
        Meta-factory. Creates a factory for a particular material model, ie. a partially specified instance of 'cls'.
        
        :param fields: Fields that corresponds to a subset of 'field_data'. Items without a corresponding entry in
            'field_data' are ignored.
        :param mesh: The mesh on (parts of) which the material model will be applied
        :param args: Additional positional arguments to the constructor
        :param field_data: Field metadata; set to 'cls.field_data' if not specified, which is a reasonable default
        :param form_compiler_parameters: FEniCS dolfin/FFC form compiler parameters for jit compilation
        :param kwargs: Additional keyword arguments to the constructor
        :return: A factory that take a ufl.Measure and keyword arguments to instantiate an object of type 'cls'
            respecting the arguments provided to 'mk_factory'
        """
        _check_factory_fields(fields, field_data if field_data is not None else cls.field_data)

        def _factory(measure: ufl.Measure, **parameters):
            parameters = {kk: vv if isinstance(vv, dolfin.Constant) else dolfin.Constant(vv)
                          for kk, vv in parameters.items()}
            return cls(dict(**fields, **parameters), mesh, measure, *args,
                       form_compiler_parameters=form_compiler_parameters, **kwargs)

        return _factory

    def __init__(self,
                 field_data: typing.Dict[str, FieldData],
                 fields: typing.Dict[str, typing.Any], mesh: dolfin.Mesh,
                 measure: ufl.Measure, force_external: typing.Iterable[str] = None, form_compiler_parameters: dict = None):
        """
        Constructor
        
        :param field_data: field metadata
        :param fields: actual fields corresponding to metadata in 'field_data'. Items without a corresponding entry in
            'field_data' are ignored.
        :param mesh: The mesh on (parts of) which the material model will be applied
        :param measure: The measure indicating the actual domain of the forms (Pi, G, G_lin and duals)
        :param force_external: Treat the given fields as external
        :param form_compiler_parameters: FEniCS dolfin/FFC form compiler parameters for jit compilation
        """
        self.field_data = field_data
        self.fields = dict([(kk, vv) for kk, vv in fields.items() if kk in self.field_data.keys()])
        self.measure = measure
        self.mesh = mesh
        self.form_compiler_parameters = form_compiler_parameters
        self._qdegree = self.measure.metadata().get("quadrature_degree", 0)
        self._quad_scheme = self.measure.metadata().get("quadrature_rule", "default")
        self._last_time = None
        self._Pi = None
        self._G = None
        self._G_lin = None
        self._evolution = None
        self._duals = None
        self.managed_states = []
        self._history_saved = False
        self._force_external = force_external if force_external is not None else []

        # Check whether all required (external) fields have been provided
        _fields_not_found = []
        for field in filter(lambda fd: fd.type not in ("InternalState", "HistoryState"), self.field_data.values()):
            if field.name not in self.fields.keys():
                _fields_not_found.append(field)
        if len(_fields_not_found) != 0:
            fields_str = ", ".join(["{:s} -> {:s}".format(field.name, field.type) for field in _fields_not_found])
            raise ValueError("Fields {:s} have not been provided.".format(fields_str))

        # Find internal data and create them on demand
        internal_fields = {
            fd.name: factories.create_coefficient(fd, mesh, self._qdegree, quad_scheme=self._quad_scheme, name=fd.name)
            for fd in self.field_data.values()
            if fd.type in ("InternalState", "HistoryState") and fd.name not in self.fields.keys()
        }
        self.managed_states += list(internal_fields.keys())
        self.fields.update(internal_fields)

        # Gather history and internal states
        internal_state_data = [
            istate for iname, istate in self.field_data.items() if istate.type == "InternalState" and iname not in self._force_external
        ]

        # Create auxiliary unified internal storage objects and update methods
        if len(internal_state_data) > 0:
            elements = [factories.mk_ufl_from_dolfin_cpp_element(self.fields[istate.name].element()) for istate in
                 internal_state_data]
            if len(elements) > 1:
                Vint = dolfin.FunctionSpace(mesh, ufl.MixedElement(elements))
            else:
                Vint = dolfin.FunctionSpace(mesh, elements[0])
                
            self.internal_storage = dolfin.Function(Vint)
            _istate_fields = [self.fields[field.name] for field in internal_state_data]
            if len(internal_state_data) > 1:
                self._write_back_internal_fields = \
                    lambda: function.assign(_istate_fields, self.internal_storage.split())
            else:
                self._write_back_internal_fields = \
                    lambda: function.assign(_istate_fields[0], self.internal_storage)
        else:
            self._write_back_internal_fields = lambda: None

    def _time_key(self) -> typing.Optional:
        if "t" in self.field_data.keys():
            return "t"
        else:
            return None

    def _time(self) -> typing.Optional:
        return float(self.fields[self._time_key()]) if self._time_key() is not None else None

    def _history_time_key(self) -> typing.Optional:
        if "t_n" in self.field_data.keys():
            return "t_n"
        else:
            return None

    def _history_time(self) -> typing.Optional:
        return float(self.fields[self._history_time_key()]) if self._history_time_key() is not None else None

    def _store_time(self):
        self._last_time = self._time()
        self._history_saved = False

    def _manage_internal_state(self):
        if self._history_time() is None:
            return

        # Time has proceeded. Updating the history time has to happen outside this class since such an operation is
        # part of a higher level time stepping scheme.
        if self._history_time() == self._last_time and not self._history_saved:
            #print("save history: {:e} == {:e}".format(self._last_time, self._history_time()), type(self))
            self._save_history()

        # The most interesting case: the time of evaluation is different from the last computed point in time
        if self._time() != self._history_time():
            #print("evolution: {:e} != {:e}".format(self._time(), self._history_time()), type(self))
            self._evolve_internal_state()
            self._store_time()

    def _save_history(self):
        # Gather history states
        history_states = [
            self.field_data[state] for state in self.managed_states if self.field_data[state].type == "HistoryState"
        ]

        # Map from history to current state
        map = dict(
            [(history_state.name, "_".join(history_state.name.split("_")[:-1])) for history_state in history_states]
        )
        for kk, vv in map.items():
            try:
                function.assign(self.fields[kk], self.fields[vv])
            except Exception as e:
                function.project_local(self.fields[vv], self.fields[kk], self._qdegree)
        self._history_saved = True

    def _evolve_internal_state(self):
        if self._evolution is not None:
            # This projection triggers the computation of the new internal states
            function.project_local(self._evolution, self.internal_storage,
                                   measure=getattr(self, "_evolution_measure", None))
            self._write_back_internal_fields()

    def _quadrature_expression(self, function: cpp.double.Function) -> dolfin.CompiledExpression:
        """
        Create a compiled (quadrature) expression that wraps the given function. This is a useful helper for the
        implementation of concrete GSMForm classes.
        """
        target_element = factories.create_quadrature_element(function.result_data, self.mesh.cell_name(),
                                                             quadrature_degree=self._qdegree, quad_scheme="default")
        all_coefficients = {kk: vv for kk, vv in self.fields.items() if not isinstance(vv, dolfin.Constant)}
        all_constants = {kk: vv for kk, vv in self.fields.items() if isinstance(vv, dolfin.Constant)}
        
        return factories.create_compiled_quadrature_expression(
            function, target_element, all_coefficients, all_constants,
            form_compiler_parameters=self.form_compiler_parameters,
            domain=self.mesh.ufl_domain()
        )
    
    def update_history(self):
        """Explicitly trigger internal book-keeping."""
        self._manage_internal_state()

    def Pi(self):
        """Volume integral of energy density or similar"""
        self._manage_internal_state()
        return _return_not_none(self._Pi)

    def G(self):
        """The weak form"""
        self._manage_internal_state()
        return _return_not_none(self._G)

    def G_lin(self):
        """The linearization of the weak form (bilinear form)"""
        self._manage_internal_state()
        return _return_not_none(self._G_lin)

    def duals(self):
        """The constitutive duals. For postprocessing."""
        #print("Duals", type(self))
        self._manage_internal_state()
        return _return_not_none(self._duals)
