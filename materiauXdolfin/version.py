# Copyright (C) 2019-2020 Matthias Rambausek
#
# This file is part of 'materiaux' (https://gitlab.com/materiaux/materiauXdolfin)
#
# SPDX-License-Identifier:    AGPL-3.0-or-later


"""Version information"""


__version__ = "0.3.0"


def check_compatibility(other_version: str) -> bool:
    return __version__ == other_version
