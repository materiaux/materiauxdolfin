import os
import re
import sys
import platform
import subprocess

from setuptools import setup, find_packages, Extension
from setuptools.command.build_ext import build_ext
from distutils.version import LooseVersion

if sys.version_info < (3, 7):
    print("Python 3.7 or higher required, please upgrade.")
    sys.exit(1)

version = {}
with open("materiauXdolfin/version.py") as fp:
    exec(fp.read(), version)

cmake_minimum_version = "3.14.0"


class CMakeExtension(Extension):
    def __init__(self, name, sourcedir=''):
        Extension.__init__(self, name, sources=[])
        self.sourcedir = os.path.abspath(sourcedir)


class CMakeBuild(build_ext):
    def run(self):
        try:
            out = subprocess.check_output(['cmake', '--version'])
            cmake_version = LooseVersion(re.search(r'version\s*([\d.]+)', out.decode()).group(1))
            if cmake_version < cmake_minimum_version:
                raise RuntimeError("CMake >= {:s} required".format(cmake_minimum_version))
        except OSError:
            raise RuntimeError("CMake (version >= {:s}) must be installed to build the following extensions: ".format(
                cmake_minimum_version) +
                               ", ".join(e.name for e in self.extensions))
        for ext in self.extensions:
            self.build_extension(ext)

    def build_extension(self, ext):
        extdir = os.path.abspath(os.path.dirname(self.get_ext_fullpath(ext.name)))
        cmake_args = ['-DCMAKE_LIBRARY_OUTPUT_DIRECTORY=' + extdir,
                      '-DPYTHON_EXECUTABLE=' + sys.executable]

        cfg = 'Debug' if self.debug else 'Release'
        build_args = ['--config', cfg]

        if platform.system() == "Windows":
            cmake_args += ['-DCMAKE_LIBRARY_OUTPUT_DIRECTORY_{}={}'.format(cfg.upper(), extdir)]
            if sys.maxsize > 2 ** 32:
                cmake_args += ['-A', 'x64']
            build_args += ['--', '/m']
        else:
            cmake_args += ['-DCMAKE_BUILD_TYPE=' + cfg]
            build_args += ['--', '-j2']

        env = os.environ.copy()
        env['CXXFLAGS'] = '{} -DVERSION_INFO=\\"{}\\"'.format(env.get('CXXFLAGS', ''),
                                                              self.distribution.get_version())
        if not os.path.exists(self.build_temp):
            os.makedirs(self.build_temp)
        subprocess.check_call(['cmake', ext.sourcedir] + cmake_args, cwd=self.build_temp, env=env)
        subprocess.check_call(['cmake', '--build', '.'] + build_args, cwd=self.build_temp)


with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='materiauXdolfin',
    version=version['__version__'],
    packages=find_packages(),
    url='https://gitlab.com/materiaux/materiauxdolfin',
    license='GNU AGPL v3',
    author='Matthias Rambausek',
    author_email='matthias.rambausek@gmail.com',
    description='materiauXdolfin establishes the connection between materiaux and dolfin via dolfincoefficients.',
    long_description=long_description,
    ext_modules=[
        CMakeExtension('materiauXdolfin.cpp'),
    ],
    cmdclass=dict(build_ext=CMakeBuild),
    install_requires=['materiaux>=0.3.0', 'dolfincoefficients>=0.2.0'],
)
