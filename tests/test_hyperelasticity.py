import typing
import pathlib
import os
import numpy as np
import materiaux.modeling.gsm as gsm
import materiaux.codegeneration.jit as jit
import materiauXdolfin.gsmforms as gsmforms
import dolfin
import ufl
from materiauXdolfin.factories import create_local_function, create_compiled_quadrature_expression
from dolfincoefficients.factories import mk_cpp_element


os.environ["DIJITSO_CACHE_DIR"] = str(pathlib.Path(__file__).parent / ".dijitso_cache")


F = gsm.State("F", shape=(3, 3))
C = F.T * F

Ic = gsm.tr(C)
J = gsm.det(F)

mu = gsm.Parameter("mu")
lmbda = gsm.Parameter("lmbda")

psi = gsm.Function("psi", (mu / 2) * (Ic - 3) - mu * gsm.ln(J) + (lmbda / 2) * (gsm.ln(J)) ** 2)
dF_psi = gsm.derivative(psi, F)
dF_dF_psi = gsm.derivative(dF_psi, F)

override = False

j_psi = jit.jit(psi, override=override, cmake_options={"CMAKE_BUILD_TYPE": "Debug"})
j_dF_psi = jit.jit(dF_psi, override=override, cmake_options={"CMAKE_BUILD_TYPE": "Debug"})
j_dF_dF_psi = jit.jit(dF_dF_psi, override=override, cmake_options={"CMAKE_BUILD_TYPE": "Debug"})
l_dF_psi = create_local_function(j_dF_psi)
l_dF_dF_psi = create_local_function(j_dF_dF_psi)


class HyperelasticityForms(gsmforms.GSMForms):
    field_data = {fd.name: fd for fd in j_psi.coefficient_data}
    field_data.update({fd.name: fd for fd in j_psi.constant_data})

    def __init__(self, fields: dict, mesh: dolfin.Mesh, measure: ufl.Measure, F_test: typing.Any, F_trial: typing.Any,
                 form_compiler_parameters: dict = None):
        super().__init__(self.field_data, fields, mesh, measure, form_compiler_parameters=form_compiler_parameters)

        i, j, k, l = ufl.indices(4)
        self._psi = self._quadrature_expression(j_psi)
        self._dF_psi = self._quadrature_expression(j_dF_psi)
        self._dF_dF_psi = self._quadrature_expression(j_dF_dF_psi)
        self._Pi = self._psi * self.measure
        self._G = self._dF_psi[i, j] * F_test[i, j] * self.measure
        self._G_lin = self._dF_dF_psi[i, j, k, l] * F_test[i, j] * F_trial[k, l] * self.measure
        self._duals = self._dF_psi


class ElasticityProblem(dolfin.NonlinearProblem):

    def __init__(self, F_func: typing.Callable[[], ufl.Form], J_func: typing.Callable[[], ufl.Form],
                 bcs: typing.Iterable[dolfin.DirichletBC]):
        super().__init__()
        self._F = F_func
        self._J = J_func
        self._bcs = bcs

    def form(self, A, P, b, x):
        pass

    def F(self, b, x):
        dolfin.assemble(self._F(), tensor=b)
        for bc in self._bcs:
            bc.apply(b, x)

    def J(self, A, x):
        dolfin.assemble(self._J(), tensor=A)
        for bc in self._bcs:
            bc.apply(A)


def test_restrict():
    mesh = dolfin.UnitCubeMesh(1, 1, 1)
    V = dolfin.VectorFunctionSpace(mesh, "CG", 1)
    u = dolfin.Function(V)
    u.interpolate(dolfin.Expression(("0.1 * x[0]", "0.2 * x[1]", "0.3 * x[2]"), degree=1))
    F_array = np.eye(3) + np.diag([0.1, 0.2, 0.3])

    coefficients = {F.name: ufl.grad(u) + ufl.Identity(3)}
    constants = {mu.name: dolfin.Constant(1.0), lmbda.name: dolfin.Constant(5.0)}

    target_element = dolfin.FiniteElement("Quadrature", mesh.cell_name(), degree=2, quad_scheme="default")
    c_psi = create_compiled_quadrature_expression(j_psi, target_element, coefficients, constants)
    target_element = dolfin.TensorElement("Quadrature", mesh.cell_name(), degree=2, quad_scheme="default",
                                          shape=tuple(j_dF_psi.result_data.shape))
    c_dF_psi = create_compiled_quadrature_expression(j_dF_psi, target_element, coefficients, constants)

    cell = dolfin.Cell(mesh, 0)
    psi_restrict = c_psi.restrict(mk_cpp_element(c_psi.ufl_element()), cell)
    for ii in range(psi_restrict.size):
        assert np.allclose(psi_restrict[ii],
                           jit.call(j_psi, F=F_array, mu=constants["mu"].values(), lmbda=constants["lmbda"].values()))

    dF_psi_restrict = c_dF_psi.restrict(mk_cpp_element(c_dF_psi.ufl_element()), cell)
    num_qpoints = dF_psi_restrict.size // j_dF_psi.result_data.size
    for ii in range(num_qpoints):
        assert np.allclose(dF_psi_restrict[ii::num_qpoints],
                           jit.call(j_dF_psi, F=F_array, mu=constants["mu"].values(),
                                    lmbda=constants["lmbda"].values()).flatten())


def test_gsm_forms():
    # the hyperelasticity dolfin demo implemented via GSMForms
    mesh = dolfin.UnitCubeMesh(12, 8, 8)
    V = dolfin.VectorFunctionSpace(mesh, "Lagrange", 2)
    left = dolfin.CompiledSubDomain("near(x[0], side) && on_boundary", side=0.0)
    right = dolfin.CompiledSubDomain("near(x[0], side) && on_boundary", side=1.0)
    c = dolfin.Constant((0.0, 0.0, 0.0))
    r = dolfin.Expression(("scale*0.0",
                           "scale*(y0 + (x[1] - y0)*cos(theta) - (x[2] - z0)*sin(theta) - x[1])",
                           "scale*(z0 + (x[1] - y0)*sin(theta) + (x[2] - z0)*cos(theta) - x[2])"),
                          scale=0.5, y0=0.5, z0=0.5, theta=np.pi / 3, degree=2)
    bcl = dolfin.DirichletBC(V, c, left)
    bcr = dolfin.DirichletBC(V, r, right)
    bcs = [bcl, bcr]

    du = dolfin.TrialFunction(V)
    v = dolfin.TestFunction(V)
    u = dolfin.Function(V)
    B = dolfin.Constant((0.0, -0.5, 0.0))
    T = dolfin.Constant((0.1, 0.0, 0.0))

    d = len(u)
    I = ufl.Identity(d)
    F = I + ufl.grad(u)

    E, nu = 10.0, 0.3
    mu, lmbda = dolfin.Constant(E / (2 * (1 + nu))), dolfin.Constant(E * nu / ((1 + nu) * (1 - 2 * nu)))

    dx = dolfin.dx(domain=mesh, metadata={"quadrature_degree": 2, "quadrature_rule": "default"})
    ds = dolfin.ds
    internal_forms = HyperelasticityForms({"F": F, "mu": mu, "lmbda": lmbda}, mesh, dx, ufl.grad(v), ufl.grad(du))
    Pi_ext = - ufl.dot(B, u) * dx - ufl.dot(T, u) * ds
    F_ext = ufl.derivative(Pi_ext, u, v)
    J_ext = ufl.derivative(F_ext, u, du)
    Pi = lambda: internal_forms.Pi() + Pi_ext
    F = lambda: internal_forms.G() + F_ext
    J = lambda: internal_forms.G_lin() + J_ext
    
    problem = ElasticityProblem(F, J, bcs)
    solver = dolfin.NewtonSolver()
    
    solver.solve(problem, u.vector())
    Pi_val = dolfin.assemble(Pi())

    u.vector().zero()
    F = I + ufl.grad(u)
    psi = (mu / 2) * (ufl.inner(F, F) - 3) - mu * ufl.ln(ufl.det(F)) + (lmbda / 2) * (ufl.ln(ufl.det(F))) ** 2
    Pi = psi * dx - ufl.dot(B, u) * dx - ufl.dot(T, u) * ds
    F = ufl.derivative(Pi, u, v)
    J = ufl.derivative(F, u, du)
    dolfin.solve(F == 0, u, bcs, J=J)
    Pi_val_expected = dolfin.assemble(Pi)
    assert abs(Pi_val - Pi_val_expected) / Pi_val_expected < 1e-12

if __name__ == "__main__":
    test_restrict()
    test_gsm_forms()
