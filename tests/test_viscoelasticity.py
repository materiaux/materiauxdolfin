import typing
import pathlib
import os
import numpy as np
from materiauXdolfin.gsmforms import GSMForms, create_evolution, create_linearization
from materiaux.modeling import gsm
from materiaux.modeling.gsm import Expr, Operator
import materiaux.cpp
import pytest
import dolfin
import ufl
from materiauXdolfin import function


os.environ["DIJITSO_CACHE_DIR"] = str(pathlib.Path(__file__).parent / ".dijitso_cache")


diag = tuple(([0, 1, 2], [0, 1, 2]))
fdiag = tuple(([0, 4, 8],))
sdiag = tuple(([0, 3, 5],))


def b(F: Expr) -> Operator:
    return F * gsm.transpose(F)


def C(F: Expr) -> Operator:
    return gsm.transpose(F) * F


def I_1(C_or_b: Expr) -> Operator:
    return gsm.tr(C_or_b)


def I_2(C_or_b: Expr) -> Operator:
    C = C_or_b
    return 1 / 2 * (gsm.tr(C) ** 2 - gsm.tr(C * C))


def I_3(C_or_b: Expr) -> Operator:
    C = C_or_b
    return gsm.det(C)


def deviator(T: Expr) -> Operator:
    return T - 1 / 3 * gsm.tr(T) * gsm.Identity(3)


def J_2(T: Expr) -> Operator:
    i, j = gsm.indices(2)
    return 1 / 2 * T[i, j] * T[i, j]


def J_2_dev(T: Expr) -> Operator:
    return J_2(deviator(T))


def be(F, Cv):
    return F * gsm.inv(Cv) * gsm.transpose(F)


def be_inv(F, Cv):
    return gsm.inv(F) * Cv * gsm.transpose(gsm.inv(F))


def psi_full_integration(I_1: Expr, I_3: Expr, mu_list: typing.Iterable[gsm.Parameter],
                         alpha_list: typing.Iterable[gsm.Parameter]):
    return sum([
        3 ** (1 - alpha) / (2 * alpha) * mu * ((I_1 - gsm.ln(I_3)) ** alpha - 3 ** alpha) if alpha != 1 else
        1 / 2 * mu * ((I_1 - gsm.ln(I_3)) - 3)
        for mu, alpha in zip(mu_list, alpha_list)
    ])


def psi_full_integration_of_C_or_b(C_or_b: Expr, mu_list: typing.Iterable[gsm.Parameter],
                                   alpha_list: typing.Iterable[gsm.Parameter]):
    return psi_full_integration(I_1(C_or_b), I_3(C_or_b), mu_list, alpha_list)


def psi_reduced_integration(I_3: Expr, mu_prime: gsm.Parameter):
    return mu_prime * (gsm.sqrt(I_3) - 1) ** 2


def psi_reduced_integration_of_C_or_b(C_or_b: Expr, mu_prime: gsm.Parameter):
    return psi_reduced_integration(I_3(C_or_b), mu_prime)


def direct_evolution_eqs(F: gsm.State, F_t: typing.Union[gsm.State, gsm.HistoryState], Cv_dot: gsm.TimeRate,
                         Cv: gsm.InternalState,
                         Cv_t: typing.Union[gsm.InternalState, gsm.HistoryState], Cv_n: gsm.HistoryState,
                         m_list: typing.Iterable[gsm.Parameter], a_list: typing.Iterable[gsm.Parameter],
                         m_prime: gsm.Parameter,
                         K_1: gsm.Parameter, K_2: gsm.Parameter, eta_0: gsm.Parameter, eta_infty: gsm.Parameter,
                         beta_1: gsm.Parameter, beta_2: gsm.Parameter):
    _C = C(F)
    _C_n = C(F_t)
    _b = b(F)
    Iv_1_n = I_1(Cv_n)
    _be = be(F, Cv)
    _be_inv = be_inv(F, Cv)
    Ie_1 = I_1(_be)
    Ie_1_n = gsm.replace(Ie_1, {Cv: Cv_n, F: F_t})
    Ie_1 = gsm.variable(Ie_1)
    Ie_3 = I_3(_be)
    Ie_3 = gsm.variable(Ie_3)
    psi_neq = psi_full_integration(Ie_1, Ie_3, m_list, a_list) + psi_reduced_integration(Ie_3, m_prime)
    dpsi_dIe1 = gsm.diff(psi_neq, Ie_1)
    Cv_n_inv = gsm.inv(Cv_n)
    Ie_2_n = 1 / 2 * (Ie_1_n ** 2 - gsm.inner(Cv_n_inv * _C_n, _C_n * Cv_n_inv))
    _A_n = 2 * gsm.replace(dpsi_dIe1, {Cv: Cv_n, F: F_t})
    J_2_neq = _A_n ** 2 * (Ie_1_n ** 2 / 3 - Ie_2_n)
    eta_k = eta_infty + (eta_0 - eta_infty + K_1 * (Iv_1_n ** beta_1 - 3 ** beta_1)) / (1 + (K_2 * J_2_neq) ** beta_2)
    vCv = gsm.variable(Cv)
    psi_neq_of_be = psi_full_integration_of_C_or_b(be(F, vCv), m_list, a_list) + \
                    psi_reduced_integration_of_C_or_b(be(F, vCv), m_prime)
    dpsi_dCv = gsm.diff(psi_neq_of_be, vCv)
    R = 2 * Cv * gsm.replace(dpsi_dCv, {Cv: Cv_t}) * Cv
    return gsm.Function("direct_evolution_equations",
                        gsm.equations({Cv: Cv_dot.discretization() + 1 / eta_k * R}))


evolution_solver = materiaux.cpp.create_newton_raphson(materiaux.cpp.DOUBLE)


class ViscoelasticMaterialKLP(GSMForms):
    import materiaux.modeling.gsm as gsm
    import materiaux.codegeneration.jit as jit

    # external states
    F = gsm.State("F", (3, 3))

    # internal states
    Cv = gsm.InternalState("Cv", (3, 3), symmetry=True)

    # history states
    F_n = gsm.HistoryState(F)
    Cv_n = gsm.HistoryState(Cv)

    # time
    t = gsm.Time()
    t_n = gsm.HistoryTime(t)

    # time rates
    Cv_dot = gsm.TimeRate(Cv, (Cv - Cv_n) / (t - t_n))

    # elastic parameters
    mu_1 = gsm.Parameter("mu_1")
    alpha_1 = gsm.Parameter("alpha_1")
    mu_2 = gsm.Parameter("mu_2")
    alpha_2 = gsm.Parameter("alpha_2")
    mu_prime = gsm.Parameter("mu_prime")

    # viscosity model parameters
    m_1 = gsm.Parameter("m_1")
    a_1 = gsm.Parameter("a_1")
    m_2 = gsm.Parameter("m_2")
    a_2 = gsm.Parameter("a_2")
    m_prime = gsm.Parameter("m_prime")
    K_1 = gsm.Parameter("K_1")
    K_2 = gsm.Parameter("K_2")
    eta_0 = gsm.Parameter("eta_0")
    eta_infty = gsm.Parameter("eta_infty")
    beta_1 = gsm.Parameter("beta_1")
    beta_2 = gsm.Parameter("beta_2")

    response = gsm.Function("energy", psi_full_integration_of_C_or_b(C(F), [mu_1, mu_2], [alpha_1, alpha_2])
                            + psi_full_integration_of_C_or_b(be(F, Cv), [m_1, m_2], [a_1, a_2]))
    eqs = direct_evolution_eqs(F, F_n, Cv_dot, Cv, Cv, Cv_n, [m_1, m_2], [a_1, a_2], m_prime, K_1, K_2, eta_0,
                               eta_infty, beta_1, beta_2)
    model = gsm.GSMBase(response, eqs)
    module_name = "viscoelasticity_klp"

    cmake_options = {"CMAKE_BUILD_TYPE": "Debug"}
    working_directory = pathlib.Path(__file__).parent / "generated"
    module_data = gsm.create_module_data(model)
    compiled_module = jit.jit_module(module_data, module_name=module_name, working_directory=working_directory,
                                     cmake_options=cmake_options)

    material_response = compiled_module.material_response
    evolution_equations = compiled_module.evolution_equations
    evolution = create_evolution(evolution_equations, solver=evolution_solver)

    field_data = {item.name: item.field_data() for item in model.all_coefficients() + model.all_constants()}
    field_data["mu_prime"] = mu_prime.field_data()
    field_data.update({item.name: item for item in evolution.coefficient_data + evolution.constant_data})

    def __init__(self, fields: dict, mesh: dolfin.Mesh, measure: dolfin.Measure, F_test: typing.Any,
                 F_trial: typing.Any,
                 form_compiler_parameters: dict = None):
        super().__init__(self.field_data, fields, mesh, measure, form_compiler_parameters=form_compiler_parameters)

        function.project_local(self.fields["F"], self.fields["F_n"])
        function.project_local(C(self.fields["F"]), self.fields["Cv"])
        function.assign(self.fields["Cv_n"], self.fields["Cv"])
        degree_under = max(0, self._qdegree - 2)
        self.measure_under = self.measure(degree=degree_under)

        F = gsm.variable(function.to_dolfin(self.fields["F"]))
        _psi = {
            self.measure: self._quadrature_expression(self.material_response.stored_energy),
            self.measure_under: psi_reduced_integration_of_C_or_b(C(F), self.fields["mu_prime"])
        }
        _duals = {
            self.measure: self._quadrature_expression(self.material_response.duals),
            self.measure_under: gsm.diff(_psi[self.measure_under], F),
        }
        _linearization = {
            self.measure: self._quadrature_expression(create_linearization(self.material_response,
                                                                           self.evolution_equations)),
            self.measure_under: gsm.diff(_duals[self.measure_under], F),
        }
        _evolution = self._quadrature_expression(self.evolution)

        i, j, k, l = gsm.indices(4)
        self._Pi = sum([vv * kk for kk, vv in _psi.items()])
        self._G = sum([vv[i, j] * F_test[i, j] * kk for kk, vv in _duals.items()])
        self._G_lin = sum([vv[i, j, k, l] * F_test[i, j] * F_trial[k, l] * kk for kk, vv in _linearization.items()])
        self._evolution = _evolution
        self._duals = sum([vv for vv in _duals.values()])


class ViscoelasticMaterialSimple(GSMForms):
    import materiaux.modeling.gsm as gsm
    import materiaux.codegeneration.jit as jit

    # external states
    F = gsm.State("F", (3, 3))

    # internal states
    Cv = gsm.InternalState("Cv", (3, 3), symmetry=True)

    # history states
    Cv_n = gsm.HistoryState(Cv)

    # time
    t = gsm.Time()
    t_n = gsm.HistoryTime(t)

    # time rates
    Cv_dot = gsm.TimeRate(Cv, (Cv - Cv_n) / (t - t_n))

    # elastic parameters
    mu = gsm.Parameter("mu")
    alpha = 1
    mu_prime = gsm.Parameter("mu_prime")

    # viscosity model parameters
    m = gsm.Parameter("m")
    a = 1
    eta = gsm.Parameter("eta")

    Cv_t_inv = gsm.inv(Cv)
    model = gsm.GeneralizedStandardMaterial(
        gsm.Function("energy",
                     psi_full_integration_of_C_or_b(C(F), [mu], [alpha])
                     + psi_full_integration_of_C_or_b(be(F, Cv), [m], [a])
                     ),
        gsm.Function("dissipation_potential", eta / 4 * (gsm.tr(Cv_dot * Cv_t_inv * Cv_dot * Cv_t_inv))),
        internal_constraints=[I_3(C(F)) - I_3(Cv)]
    )
    module_name = "viscoelasticity_simple"

    cmake_options = {"CMAKE_BUILD_TYPE": "Debug"}
    working_directory = pathlib.Path(__file__).parent / "generated"

    module_data = gsm.create_module_data(model)
    compiled_module = jit.jit_module(module_data, module_name=module_name,
                                     working_directory=working_directory,
                                     cmake_options=cmake_options)

    material_response = compiled_module.material_response
    evolution_equations = compiled_module.evolution_equations
    evolution = create_evolution(evolution_equations, solver=evolution_solver)

    field_data = {item.name: item.field_data() for item in model.all_coefficients() + model.all_constants()}
    field_data["mu_prime"] = mu_prime.field_data()
    field_data.update({item.name: item for item in evolution.coefficient_data + evolution.constant_data})

    def __init__(self, fields: dict, mesh: dolfin.Mesh, measure: dolfin.Measure, F_test: typing.Any,
                 F_trial: typing.Any,
                 form_compiler_parameters: dict = None):
        super().__init__(self.field_data, fields, mesh, measure, form_compiler_parameters=form_compiler_parameters)

        function.project_local(C(self.fields["F"]), self.fields["Cv"])
        function.assign(self.fields["Cv_n"], self.fields["Cv"])
        degree_under = max(0, self._qdegree - 2)
        self.measure_under = self.measure(degree=degree_under)

        F = gsm.variable(function.to_dolfin(self.fields["F"]))
        _psi = {
            self.measure: self._quadrature_expression(self.material_response.stored_energy),
            self.measure_under: psi_reduced_integration_of_C_or_b(C(F), self.fields["mu_prime"])
        }
        _duals = {
            self.measure: self._quadrature_expression(self.material_response.duals),
            self.measure_under: gsm.diff(_psi[self.measure_under], F),
        }
        _linearization = {
            self.measure: self._quadrature_expression(create_linearization(self.material_response,
                                                                           self.evolution_equations)),
            self.measure_under: gsm.diff(_duals[self.measure_under], F),
        }
        _evolution = self._quadrature_expression(create_evolution(self.evolution_equations, solver=evolution_solver))

        i, j, k, l = gsm.indices(4)
        self._Pi = sum([vv * kk for kk, vv in _psi.items()])
        self._G = sum([vv[i, j] * F_test[i, j] * kk for kk, vv in _duals.items()])
        self._G_lin = sum([vv[i, j, k, l] * F_test[i, j] * F_trial[k, l] * kk for kk, vv in _linearization.items()])
        self._evolution = _evolution
        self._duals = sum([vv for vv in _duals.values()])


class MaterialResponse:
    def __init__(self, incremental_potential_module):
        self.stored_energy = incremental_potential_module.incremental_potential
        self.duals = incremental_potential_module.d_ext_incremental_potential
        self.d_int_duals = incremental_potential_module.d_int_d_ext_incremental_potential
        self.d_ext_duals = incremental_potential_module.d_ext_d_ext_incremental_potential


class EvolutionEquations:
    def __init__(self, incremental_potential_module):
        self.eqs = incremental_potential_module.d_int_incremental_potential
        self.d_int_eqs = incremental_potential_module.d_int_d_int_incremental_potential
        self.d_ext_eqs = incremental_potential_module.d_ext_d_int_incremental_potential


class ViscoelasticMaterialSimpleIncr(GSMForms):
    import materiaux.modeling.gsm as gsm
    import materiaux.codegeneration.jit as jit

    # external states
    F = gsm.State("F", (3, 3))

    # internal states
    Cv = gsm.InternalState("Cv", (3, 3), symmetry=True)

    # history states
    Cv_n = gsm.HistoryState(Cv)

    # time
    t = gsm.Time()
    t_n = gsm.HistoryTime(t)

    # time rates
    Cv_dot = gsm.TimeRate(Cv, (Cv - Cv_n) / (t - t_n))

    # elastic parameters
    mu = gsm.Parameter("mu")
    alpha = 1
    mu_prime = gsm.Parameter("mu_prime")

    # viscosity model parameters
    m = gsm.Parameter("m")
    a = 1
    eta = gsm.Parameter("eta")

    Cv_t_inv = gsm.inv(Cv_n)

    def mk_integrator(t, t_n):
        return lambda f: f * (t - t_n)

    model = gsm.IncrementalMaterialPotential(
        gsm.Function("energy",
                     psi_full_integration_of_C_or_b(C(F), [mu], [alpha])
                     + psi_full_integration_of_C_or_b(be(F, Cv), [m], [a])
                     ),
        gsm.Function("dissipation_potential", eta / 4 * (gsm.tr(Cv_dot * Cv_t_inv * Cv_dot * Cv_t_inv))),
        mk_integrator(t, t_n),
        internal_constraints=[I_3(C(F)) - I_3(Cv)]
    )
    module_name = "viscoelasticity_simple_incr"

    cmake_options = {"CMAKE_BUILD_TYPE": "Debug"}
    working_directory = pathlib.Path(__file__).parent / "generated"

    module_data = gsm.create_module_data(model)
    compiled_module = jit.jit_module(module_data, module_name=module_name,
                                     working_directory=working_directory,
                                     cmake_options=cmake_options)

    material_response = MaterialResponse(compiled_module)
    evolution_equations = EvolutionEquations(compiled_module)
    evolution = create_evolution(evolution_equations, solver=evolution_solver)

    field_data = {item.name: item.field_data() for item in model.all_coefficients() + model.all_constants()}
    field_data["mu_prime"] = mu_prime.field_data()
    field_data.update({item.name: item for item in evolution.coefficient_data + evolution.constant_data})

    def __init__(self, fields: dict, mesh: dolfin.Mesh, measure: dolfin.Measure, F_test: typing.Any,
                 F_trial: typing.Any,
                 form_compiler_parameters: dict = None):
        super().__init__(self.field_data, fields, mesh, measure, form_compiler_parameters=form_compiler_parameters)

        function.project_local(C(self.fields["F"]), self.fields["Cv"])
        function.assign(self.fields["Cv_n"], self.fields["Cv"])
        degree_under = max(0, self._qdegree - 2)
        self.measure_under = self.measure(degree=degree_under)

        F = gsm.variable(function.to_dolfin(self.fields["F"]))
        _psi = {
            self.measure: self._quadrature_expression(self.material_response.stored_energy),
            self.measure_under: psi_reduced_integration_of_C_or_b(C(F), self.fields["mu_prime"])
        }
        _duals = {
            self.measure: self._quadrature_expression(self.material_response.duals),
            self.measure_under: gsm.diff(_psi[self.measure_under], F),
        }
        _linearization = {
            self.measure: self._quadrature_expression(create_linearization(self.material_response,
                                                                           self.evolution_equations)),
            self.measure_under: gsm.diff(_duals[self.measure_under], F),
        }
        _evolution = self._quadrature_expression(create_evolution(self.evolution_equations, solver=evolution_solver))

        i, j, k, l = gsm.indices(4)
        self._Pi = sum([vv * kk for kk, vv in _psi.items()])
        self._G = sum([vv[i, j] * F_test[i, j] * kk for kk, vv in _duals.items()])
        self._G_lin = sum([vv[i, j, k, l] * F_test[i, j] * F_trial[k, l] * kk for kk, vv in _linearization.items()])
        self._evolution = _evolution
        self._duals = sum([vv for vv in _duals.values()])


class ViscoElasticityProblem(dolfin.NonlinearProblem):

    def __init__(self, F_func: typing.Callable[[], ufl.Form], J_func: typing.Callable[[], ufl.Form],
                 bcs: typing.Iterable[dolfin.DirichletBC]):
        super().__init__()
        self._F = F_func
        self._J = J_func
        self._bcs = bcs

    def form(self, A, P, b, x):
        pass

    def F(self, b, x):
        dolfin.assemble(self._F(), tensor=b)
        for bc in self._bcs:
            bc.apply(b, x)

    def J(self, A, x):
        dolfin.assemble(self._J(), tensor=A)
        for bc in self._bcs:
            bc.apply(A)


_MN = _MPa = 1
_kPa = _kPas = 1e-3
_Pa = _Pas = 1e-6

newton_params = {"newton_max_iter": 10, "newton_tol": 1e-6}

params = {
    ViscoelasticMaterialKLP: {
        "mu_1": 1.08,
        "alpha_1": 0.26,
        "mu_2": 0.017,
        "alpha_2": 7.68,
        "mu_prime": 5e2,
        "m_1": 1.57,
        "a_1": -10,
        "m_2": 0.59,
        "a_2": 7.53,
        "m_prime": 5e2,
        "K_1": 442,
        "K_2": 1289.49,
        "eta_0": 2.11,
        "eta_infty": 0.1,
        "beta_1": 3,
        "beta_2": 1.929,
        **newton_params
    },
    ViscoelasticMaterialSimple: {
        "mu": 5 * _kPa,
        "m": 20 * _kPa,
        "eta": 20 * _Pas,
        "mu_prime": 1 * _MPa,
        **newton_params
    }
}
params[ViscoelasticMaterialSimpleIncr] = params[ViscoelasticMaterialSimple]

expected = {
    ViscoelasticMaterialKLP: {
        "F": np.array([1.05409255, 1.05409255, 0.9]),
        "Cv": np.array([1.0957018, 1.0957018, 0.83271685]),
        "S": np.array([2.00978123e-13, 2.00797712e-13, -4.33454381e-01])
    },
    ViscoelasticMaterialSimple: {
        "F": np.array([1.05409255, 1.05409255, 0.9]),
        "Cv": np.array([1.10466868, 1.10466868, 0.81902335]),
        "S": np.array([6.48867895e-16, 6.58219139e-16, -1.74498512e-03])
    },
    ViscoelasticMaterialSimpleIncr: {
        "F": np.array([1.05409255, 1.05409255, 0.9]),
        "Cv": np.array([1.10467934, 1.10467934, 0.81900752]),
        "S": np.array([-2.10335221e-16, -2.06865775e-16, -1.74433547e-03])
    }
}


@pytest.mark.parametrize("material_class,stretch_rate", ((ViscoelasticMaterialKLP, 0.00023),
                                                         (ViscoelasticMaterialSimple, 1.0),
                                                         (ViscoelasticMaterialSimpleIncr, 1.0)))
def test_gsm_forms(material_class, stretch_rate):
    # a homogeneous FEM problem for comparison with the evolution at a single point

    mesh = dolfin.UnitCubeMesh(1, 1, 1)
    dx = dolfin.dx(domain=mesh, metadata={"quadrature_degree": 2})

    elmt_u = dolfin.VectorElement("CG", mesh.cell_name(), 2)
    elmt_S = dolfin.TensorElement("DG", mesh.cell_name(), 0)
    V = dolfin.FunctionSpace(mesh, elmt_u)
    V_S = dolfin.FunctionSpace(mesh, elmt_S)
    u = dolfin.Function(V, name="solution")

    t = dolfin.Constant(0)
    t_n = dolfin.Constant(0)

    dstretch = dolfin.Constant(0)
    bcs = [
        dolfin.DirichletBC(V.sub(2), dolfin.Constant(0), lambda x, onb: x[2] == 0),
        dolfin.DirichletBC(V, dolfin.Constant(np.zeros(3)), lambda x, onb: np.allclose(x, np.zeros(3)),
                           method='pointwise'),
        dolfin.DirichletBC(V.sub(2), dstretch, lambda x, onb: x[2] == 1),
    ]

    fields = {"F": ufl.Identity(3) + ufl.grad(u), "t": t, "t_n": t_n}
    test_F = ufl.grad(ufl.TestFunction(V))
    trial_F = ufl.grad(ufl.TrialFunction(V))
    factory = material_class.mk_factory(fields, mesh, test_F, trial_F)

    material_forms = factory(dx, **params[material_class])

    F = lambda: material_forms.G()
    J = lambda: material_forms.G_lin()

    target_stretch = 0.9
    n_steps = 25
    stretch = 1.0
    direction = 1 if target_stretch > 1 else -1
    dt = np.abs(target_stretch - 1) / stretch_rate / n_steps
    F_data = np.zeros((3, n_steps * 2))
    Cv_data = np.zeros((3, n_steps * 2))
    S_data = np.zeros((3, n_steps * 2))

    u.vector().zero()
    for step in range(n_steps * 2):
        if step >= n_steps and stretch_rate != 0:
            stretch_rate = 0
        stretch += direction * stretch_rate * dt
        dstretch.assign(stretch - 1)
        print("stretch: ", dstretch.values())
        t.assign(t.values() + dt)

        problem = ViscoElasticityProblem(F, J, bcs)
        solver = dolfin.NewtonSolver()
        solver.solve(problem, u.vector())
        t_n.assign(t)

        F_data[:, step] = np.array([1 / np.sqrt(stretch), 1 / np.sqrt(stretch), stretch])
        Cv_data[:, step] = material_forms.fields["Cv_n"].dolfin_coefficient().vector().get_local()[:6][sdiag]
        S_data[:, step] = function.project_local(
            material_forms.duals(), V_S,
            measure=material_forms.measure
        ).vector().get_local()[:9][fdiag]

    if __name__ == "__main__":
        return F_data, Cv_data, S_data
    assert np.allclose(F_data[:, n_steps - 1], expected[material_class]["F"], atol=1e-8, rtol=1e-8)
    assert np.allclose(Cv_data[:, n_steps - 1], expected[material_class]["Cv"], atol=1e-8, rtol=1e-8)
    assert np.allclose(S_data[:, n_steps - 1], expected[material_class]["S"], atol=1e-8, rtol=1e-8)


if __name__ == "__main__":
    data_klp = test_gsm_forms(ViscoelasticMaterialKLP, 0.00023)
    data_simple = test_gsm_forms(ViscoelasticMaterialSimple, 1.0)
    data_simple_incr = test_gsm_forms(ViscoelasticMaterialSimpleIncr, 1.0)
